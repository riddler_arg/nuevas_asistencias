<?php declare(strict_types=1);

namespace Riddler\Asistencias\Handlers;

class SessionTest extends \PHPUnit\Framework\TestCase {

    protected function setUp() : void {
//        echo PHP_EOL;
    }

    public function testValidateWithValidArguments() : void {
        $out = SessionHandler::validateParameters('user', '1234');
        $this->assertEmpty($out);
    }

    public function testValidateWithInvalidUser() : void {
        $out = SessionHandler::validateParameters('', '1234');
        $this->assertCount(1, $out);
        $this->assertEquals('Username too short', $out[0]);
    }

    public function testValidateWithInvalidPass() : void {
        $out = SessionHandler::validateParameters('user', '');
        $this->assertCount(1, $out);
        $this->assertEquals('Password too short', $out[0]);
    }

    public function testValidateWithInvalidUserAndPass() : void {
        $out = SessionHandler::validateParameters('', '');
        $this->assertCount(2, $out);
        $this->assertEquals('Username too short', $out[0]);
        $this->assertEquals('Password too short', $out[1]);
    }

    public function testValidateWithNullUser() : void {
        $out = SessionHandler::validateParameters(null, '1234');
        $this->assertCount(1, $out);
        $this->assertEquals('Username can\'t be null', $out[0]);
    }

    public function testValidateWithNullPass() : void {
        $out = SessionHandler::validateParameters('user', null);
        $this->assertCount(1, $out);
        $this->assertEquals('Password can\'t be null', $out[0]);
    }

    public function testValidateWithNullUserAndPass() : void {
        $out = SessionHandler::validateParameters(null, null);
        $this->assertCount(2, $out);
        $this->assertEquals('Username can\'t be null', $out[0]);
        $this->assertEquals('Password can\'t be null', $out[1]);
    }
}