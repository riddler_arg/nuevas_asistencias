<?php declare(strict_types=1);

namespace Riddler\Asistencias\Utils;

class CryptorTest extends \PHPUnit\Framework\TestCase {

    protected function setUp() : void {
//        echo PHP_EOL;
    }

    public function testDecryptWithInvalidArgument() : void {
        $foo = Cryptor::decrypt('Hello');
        $this->assertEmpty($foo);
    }

    public function testDecryptWithNullArgument() : void {
        $this->expectException(\TypeError::class);
        Cryptor::decrypt(null);
    }

    public function testEncryptWithNullArgument() : void {
        $this->expectException(\TypeError::class);
        Cryptor::encrypt(null);
    }

    public function testEncryptWithNumericArgument() {
        $this->expectException(\TypeError::class);
        Cryptor::encrypt(123);
    }

    public function testEncryptDecrypt() : void {
        $foo = Utils::__rand_str__(512);
        $enc = Cryptor::encrypt($foo);
        $this->assertTrue(ctype_xdigit($enc));
        $this->assertNotEquals($foo, $enc);
        $this->assertEquals($foo, Cryptor::decrypt($enc));
    }

    public function testEncryptDecryptWithManySizes() : void {
        for ($i = 0; $i < 256; $i++) {
            $foo = Utils::__rand_str__($i);
            $enc = Cryptor::encrypt($foo);
            $this->assertTrue(ctype_xdigit($enc));
            $this->assertNotEquals($foo, $enc);
            $this->assertEquals($foo, Cryptor::decrypt($enc));
        }
    }

    public function testEncryptDecryptWithUncommonSymbols() : void {
        $foo = 'Ô>eÃãµÓ>N9=K±º&ß$så}[½?]ô4;Ýt§f('
              .'Q±êäÓÚ#3ÍH§^,*½A)CÐ*±¦(iìm*ó_iMª'
              .'ß³kD²©m+ý¬-w}Ý+9ÆÀXé/yÐP¼½®åñ3,W'
              .'â®ÖóÃÑ«ÉäóÖôÃ½íÿ¢½öÂÞ¤òÓ¨¡ç»ôÞÚÉ';
        $enc = Cryptor::encrypt($foo);
        $this->assertTrue(ctype_xdigit($enc));
        $this->assertNotEquals($foo, $enc);
        $this->assertEquals($foo, Cryptor::decrypt($enc));
    }

    public function testEncryptDecryptWithEmojis() : void {
        $foo = '😄😊😉😍😘😚😜😝😳😁😣😢😂😭';
        $enc = Cryptor::encrypt($foo);
        $this->assertTrue(ctype_xdigit($enc));
        $this->assertNotEquals($foo, $enc);
        $this->assertEquals($foo, Cryptor::decrypt($enc));
    }

    public function testEncryptDecryptWithEmptyInput() : void {
        $foo = '';
        $enc = Cryptor::encrypt($foo);
        $this->assertTrue(ctype_xdigit($enc));
        $this->assertNotEquals($foo, $enc);
        $this->assertEquals($foo, Cryptor::decrypt($enc));
    }

}
