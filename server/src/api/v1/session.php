<?php declare(strict_types=1);

require_once (__DIR__ . '/../../autoload.php');

use Riddler\Asistencias\Utils\HttpStatusCode;

header('Content-Type: application/json');

if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') !== 'POST') {
    echo json_encode(['response' => 'Invalid request']);
    http_response_code(HttpStatusCode::METHOD_NOT_ALLOWED);
    exit(0);
}