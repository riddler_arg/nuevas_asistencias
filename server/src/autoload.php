<?php declare(strict_types=1);

spl_autoload_register(function (string $class) {
    $foo = explode('\\', strtolower($class));
    if ($foo[0] === 'riddler' and $foo[1] === 'asistencias') {
        $isBase = $foo[2] === 'base' ? 3 : 2;
        require_once (__DIR__ . '/core/' . implode('/', array_slice($foo, $isBase)) . '.class.php');
    }
});

require (__DIR__ . '/../vendor/autoload.php');