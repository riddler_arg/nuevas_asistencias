<?php declare(strict_types=1);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Riddler\Asistencias\Handlers;

/**
 * Description of RequestHandler
 *
 * @author fede
 */
abstract class RequestHandler {
    private $methods;

    public function __construct(array $methods = []) {
        $this->methods = $methods;
    }

    public function isValidMethod() {
        $method = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        return in_array($method, $this->methods);
    }
}
