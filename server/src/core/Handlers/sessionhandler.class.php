<?php declare(strict_types=1);

namespace Riddler\Asistencias\Handlers;

use Riddler\Asistencias\Base\Database;

class SessionHandler extends RequestHandler {
    public static function validateParameters(?string $user, ?string $pass) : array {
        $out = [];

        if (!isset($user) || null === $user) {
            array_push($out, 'Username can\'t be null');
        } else if (strlen(trim($user)) < 4) {
            array_push($out, 'Username too short');
        }

        //Acá podés buscar herramientas como zxcvbn que te sirven para validar
        //complejidad de contraseñas
        if (!isset($pass) || null === $pass) {
            array_push($out, 'Password can\'t be null');
        } else if (strlen(trim($pass)) < 4) {
            array_push($out, 'Password too short');
        }
        
        return $out;
    }

    public static function login(string $user, string $pass) : int {
        $sql = 'SELECT `id`, `pass` FROM `users` WHERE `user` = :user';
        try {
            $stmt = Database::get()->prepare($sql);
            $stmt->execute([
                ':user' => $user
            ]);

            if ($stmt->rowCount() === 0) {
                return -1; // Invalid User
            }

            $obj = $stmt->fetchObject();
            if (password_verify($pass, $obj->pass)) {
                return $obj->id;
            }

            return -2; // Invalid Password
        } catch (Exception $ex) {
            error_log($ex->getMessage());
            return 0;
        }
    }

    public static function newUser(string $user, string $pass) : int {
        $sql = 'INSERT INTO `users` (`user`, `pass`) VALUES (:user, :pass)';
        try {
            $hash = password_hash($pass, PASSWORD_BCRYPT);
            $stmt = Database::get()->prepare($sql);
            $stmt->execute([
                ':user' => $user,
                ':pass' => $hash
            ]);
            return Database::get()->lastInsertId();
        } catch (Exception $ex) {
            error_log($ex->getMessage());
            return 0;
        }
    }
}
