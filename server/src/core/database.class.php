<?php declare(strict_types=1);

namespace Riddler\Asistencias\Base;

class Database {
    private static $CONNECTION;

    /**
     * Retrieves a connection with the database
     *
     * @return \PDO connection
     * @author Federico Vera <fede@riddler.com.ar>
     */
    static public function get() : \PDO {
        if (isset(Database::$CONNECTION)) {
            return Database::$CONNECTION;
        }

        try {
            $dbhost = Config::DB_HOST;
            $dbname = Config::DB_NAME;
            $complh = "mysql:$dbhost;dbname=$dbname;charset=utf8mb4";
            Database::$CONNECTION = new \PDO(
                $complh,
                Config::DB_USER,
                Config::DB_PASS,
                [
                    \PDO::ATTR_PERSISTENT => TRUE,
                    \PDO::ATTR_ERRMODE    => \PDO::ERRMODE_EXCEPTION
                ]
            );
            return Database::$CONNECTION;
        } catch (\PDOException $e) {
            Log::error('database', 'Unable to connect to MySQL', [], $e->getMessage());
            http_response_code(500);
            exit(1);
        }
    }
}
