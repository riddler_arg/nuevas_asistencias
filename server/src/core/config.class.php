<?php declare(strict_types=1);

namespace Riddler\Asistencias\Base;

abstract class Config {
    # Database Host
    const DB_HOST    = 'unix_socket=/var/run/mysqld/mysqld.sock';
    # Database Name
    const DB_NAME    = 'YOUR_DATABASE_NAME_HERE';
    # Database Username
    const DB_USER    = 'YOUR_USERNAME_HERE';
    # Database Password
    const DB_PASS    = 'YOUR_PASSWORD_HERE';

    # Application base url (it's used in caches)
    const APP_BASE   = 'url.of.your.site';
    # Application URL (we usually include the https)
    const APP_URL    = '//url.of.your.site/';
    # Application path in the server
    const APP_PATH   = '/path/to/site/';

    # Password used as general encryption key
    const ENCRYPTION_PASS = 'Y0Ur_Pa55_g0_h3r3';
}